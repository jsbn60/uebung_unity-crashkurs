﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

[RequireComponent(typeof(Rigidbody))]
public class CameraMovement : MonoBehaviour
{
    /* Components */
    private Rigidbody rb;
    
    /* Movement */
    private Vector2 rotation;
    [SerializeField] private float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rotation = new Vector2(0,0);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(transform.forward);
        }
        
        if (Input.GetKey(KeyCode.S))
        {
            rb.AddForce(-transform.forward);
        }
        
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(transform.right);
        }
        
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(-transform.right);
        }
        
        rotation.y += Input.GetAxis ("Mouse X");
        rotation.x += -Input.GetAxis ("Mouse Y");
        transform.eulerAngles = rotation * speed;
    }
}

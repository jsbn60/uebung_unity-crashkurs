﻿using UnityEngine;

namespace Übung_2.Scripts
{
    /// <summary>
    /// This class represents a canon that is able to shoot canonballs.
    /// </summary>
    public class Canon : MonoBehaviour
    {
        /// <summary>
        /// Prefab of canonball.
        /// </summary>
        [SerializeField] private Rigidbody canonBallPrefab;
        
        /// <summary>
        /// Spawnpoint for canonballs.
        /// </summary>
        [SerializeField] private Transform spawnPoint;
        
        /// <summary>
        /// Force to shot the canonballs with.
        /// </summary>
        [SerializeField] private float shotForce;


        // Update is called once per frame
        void Update()
        {
        /*
         * IMPLEMENT SHOOTING OF CANONBALL HERE
         */
        }
    }
}
